//
//  ForecastDatesCollectionViewAdapter.swift
//  Forecast5
//
//  Created by Simon on 25/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import UIKit

class ForecastDatesCollectionViewAdapter: NSObject {
    
    private var collectionView: UICollectionView!
    private var dailyForecasts = [DailyForecast]()
    private var dailyForecastChanged: ((DailyForecast)->Void)?
    private var selectedDailyForecast: DailyForecast? {
        didSet {
            if let selectedDailyForecast = self.selectedDailyForecast {
                self.dailyForecastChanged?(selectedDailyForecast)
            }
        }
    }
    
    func set(collectionView: UICollectionView, dailyForecastChanged: @escaping ((DailyForecast)->Void)) {
        self.collectionView = collectionView
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.dailyForecastChanged = dailyForecastChanged
        
        let nib = UINib(nibName: "ForecastDatesCollectionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "ForecastDatesCollectionViewCell")
    }
    
    func set(dailyForecasts: [DailyForecast]) {
        self.dailyForecasts = dailyForecasts
        self.selectedDailyForecast = dailyForecasts.first
        self.collectionView.reloadData()
    }
}

extension ForecastDatesCollectionViewAdapter: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dailyForecasts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastDatesCollectionViewCell", for: indexPath) as! ForecastDatesCollectionViewCell
        cell.set(date: self.dailyForecasts[indexPath.item].date)
        return cell
    }
}

extension ForecastDatesCollectionViewAdapter: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedDailyForecast = self.dailyForecasts[indexPath.item]
    }
}
