//
//  ForecastDataStore.swift
//  Forecast5
//
//  Created by Simon on 25/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

class ForecastDataStore {
    
    func getForecast(for city: City, completion: @escaping (([DailyForecast])->Void)) {
        let request = OWMNetworkServicesModels.getForecast.request(city: city)
        OWMNetworkServices.shared.getForecast(request: request) { (response) in
            func completionEmpty() {
                completion([])
            }
            guard let rawWeatherForecastData = response.rawWeatherForecastData else {
                completionEmpty()
                return
            }
            guard let list = rawWeatherForecastData["list"] as? [[String:Any]] else {
                completionEmpty()
                return
            }
            let dailyForecasts = self.getDailyForecasts(from: list)
            completion(dailyForecasts)
        }
    }
    
    /*
     1. Separate out the forecast by dates
     2. Sort out the date wise forecast by hours
     3.
     */
    
    private func getDailyForecasts(from list: [[String:Any]]) -> [DailyForecast] {
        let uniqueDates = self.getUniqueDates(from: list)
        var dailyForecasts = [DailyForecast]()
        for date in uniqueDates {
            let hourlyForecasts = self.getHourlyForecasts(for: date, from: list)
            let dailyForecast = DailyForecast(date: date, hourlyForecasts: hourlyForecasts)
            dailyForecasts.append(dailyForecast)
        }
        return dailyForecasts
    }
    
    private func getUniqueDates(from list: [[String:Any]]) -> [Date] {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        var dates = [Date]()
        _ = list.filter { (rawWeatherForecastSingleObject) -> Bool in
            if let dateString = rawWeatherForecastSingleObject["dt_txt"] as? String,
                let date = dateformatter.date(from: dateString),
                let dateWithoutTime = date.getDateWithoutTime() {
                
                dates.append(dateWithoutTime)
            }
            return false
        }
        let uniqueDates = Set(dates)
        let sortedUniqueDates = Array(uniqueDates).sorted { (date1, date2) -> Bool in
            return date1 < date2
        }
        return sortedUniqueDates
    }
    
    func getHourlyForecasts(for date: Date, from list: [[String:Any]]) -> [HourlyForecast] {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateformatter.timeZone = TimeZone.init(abbreviation: "BST")
        
        var hourlyForecasts = [HourlyForecast]()
        
        for rawWeatherForecastSingleObject in list {
            if let dateString = rawWeatherForecastSingleObject["dt_txt"] as? String,
                let dateTemp = dateformatter.date(from: dateString),
                dateTemp.isSameDay(to: date) == true,
                let hour = dateTemp.getHour(),
                let minutes = dateTemp.getMinutes() {
                
                let forecast = self.getForecast(from: rawWeatherForecastSingleObject)
                let hourlyForecast = HourlyForecast(hour: hour, minutes: minutes, forecast: forecast)
                hourlyForecasts.append(hourlyForecast)
            }
        }
        return hourlyForecasts
    }
    
    func getForecast(from rawForecast: [String:Any]) -> Forecast {
        
        let weather = self.getWeather(from: rawForecast)
        let clouds = self.getClouds(from: rawForecast)
        let wind = self.getWind(from: rawForecast)
        let temperature = self.getTemperature(from: rawForecast)
        let pressure = self.getPressure(from: rawForecast)
        let level = self.getLevel(from: rawForecast)
        let humidity = self.getHumidity(from: rawForecast)
        
        let forecast = Forecast(
            weather: weather,
            clouds: clouds,
            wind: wind,
            temperature: temperature,
            pressure: pressure,
            level: level,
            humidity: humidity)
        return forecast
    }
    
    private func getWeather(from rawForecast: [String:Any]) -> Forecast.Weather? {
        guard let weatherList = rawForecast["weather"] as? [[String:String]],
            let rawWeather = weatherList.first,
            let displayText = rawWeather["main"],
            let description = rawWeather["description"] else {
                return nil
        }
        let weather = Forecast.Weather(displayText: displayText, description: description)
        return weather
    }
    
    private func getClouds(from rawForecast: [String:Any]) -> Forecast.Clouds? {
        guard let rawCloud = rawForecast["clouds"] as? [String:Double],
            let percentage = rawCloud["all"] else {
                return nil
        }
        let cloud = Forecast.Clouds(percentage: percentage)
        return cloud
    }
    
    private func getWind(from rawForecast: [String:Any]) -> Forecast.Wind? {
        guard let rawWind = rawForecast["wind"] as? [String:Double],
            let speed = rawWind["speed"],
            let degree = rawWind["deg"] else {
                return nil
        }
        let wind = Forecast.Wind(speed: speed, degree: degree)
        return wind
    }
    
    private func getTemperature(from rawForecast: [String:Any]) -> Forecast.Temperature? {
        guard let rawMain = rawForecast["main"] as? [String:Double],
            let value = rawMain["temp"],
            let maximum = rawMain["temp_max"],
            let minimum = rawMain["temp_min"] else {
                return nil
        }
        let temperature = Forecast.Temperature(value: value, minimum: minimum, maximum: maximum)
        return temperature
    }
    
    private func getPressure(from rawForecast: [String:Any]) -> Forecast.Pressure? {
        guard let rawMain = rawForecast["main"] as? [String:Double],
            let value = rawMain["pressure"] else {
                return nil
        }
        let pressure = Forecast.Pressure(value: value)
        return pressure
    }
    
    private func getLevel(from rawForecast: [String:Any]) -> Forecast.Level? {
        guard let rawMain = rawForecast["main"] as? [String:Double],
            let sea = rawMain["sea_level"],
            let ground = rawMain["grnd_level"] else {
                return nil
        }
        let level = Forecast.Level(sea: sea, ground: ground)
        return level
    }
    
    private func getHumidity(from rawForecast: [String:Any]) -> Forecast.Humidity? {
        guard let rawMain = rawForecast["main"] as? [String:Double],
            let value = rawMain["humidity"] else {
                return nil
        }
        let humidity = Forecast.Humidity(value: value)
        return humidity
    }
}
