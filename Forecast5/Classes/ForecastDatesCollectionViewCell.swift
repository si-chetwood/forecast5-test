//
//  ForecastDatesCollectionViewCell.swift
//  Forecast5
//
//  Created by Simon on 25/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import UIKit

class ForecastDatesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    func set(date: Date) {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "EEE\ndd"
        dateformatter.timeZone = TimeZone(abbreviation: "BST")
        self.dateLabel.text = dateformatter.string(from: date)
    }

}
