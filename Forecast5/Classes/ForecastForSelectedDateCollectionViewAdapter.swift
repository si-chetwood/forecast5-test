//
//  ForecastForSelectedDateCollectionViewAdapter.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import UIKit

class ForecastForSelectedDateCollectionViewAdapter: NSObject {

    private var collectionView: UICollectionView!
    private var dailyForecast: DailyForecast?
    
    func set(collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let nib = UINib(nibName: "ForecastCollectionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "ForecastCollectionViewCell")
    }
    
    func set(dailyForecast: DailyForecast) {
        self.dailyForecast = dailyForecast
        self.reloadData()
    }
    
    func reloadData() {
        self.collectionView.setContentOffset(CGPoint.zero, animated: true)
        self.collectionView.reloadData()
    }
}

extension ForecastForSelectedDateCollectionViewAdapter: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dailyForecast?.hourlyForecasts.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCollectionViewCell", for: indexPath) as! ForecastCollectionViewCell
        if let hourlyForecast = self.dailyForecast?.hourlyForecasts[indexPath.item] {
            cell.set(hourlyForecast: hourlyForecast)
        }
        return cell
    }
}

extension ForecastForSelectedDateCollectionViewAdapter: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView.bounds.size
    }
}

extension ForecastForSelectedDateCollectionViewAdapter: UICollectionViewDelegate {
    
}
