//
//  ForecastCollectionViewCell.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var minimumTemperatureLabel: UILabel!
    @IBOutlet weak var maximumTemperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var weatherBackgroundImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(hourlyForecast: HourlyForecast) {
        
        let forecast = hourlyForecast.forecast
        
        if let temperature = forecast.temperature {
            self.set(temperature: temperature.value, to: self.temperatureLabel)
            self.set(temperature: temperature.minimum, to: self.minimumTemperatureLabel)
            self.set(temperature: temperature.maximum, to: self.maximumTemperatureLabel)
        } else {
            self.temperatureLabel.text = "N.A"
            self.minimumTemperatureLabel.text = "N.A"
            self.maximumTemperatureLabel.text = "N.A"
        }
        
        if let pressure = forecast.pressure,
            let value = pressure.value.trimAtEnd(digits: 0) {
            self.pressureLabel.text = "\(value) hPa"
        }
        
        if let humidity = forecast.humidity,
            let value = humidity.value.trimAtEnd(digits: 0) {
            self.humidityLabel.text = "\(value) %"
        }
        
        if let hour = hourlyForecast.hour.minimumDigits(2),
            let min = hourlyForecast.minutes.minimumDigits(2) {
            self.timeLabel.text = "\(hour):\(min)"
        }
    }
    
    private func set(temperature: Double, to label: UILabel) {
        if let celsius = temperature.trimAtEnd(digits: 0) {
            label.text = "\(celsius)º C"
        }
    }
    
//    private func convertToCelsius(from kelvin: Double) -> Double {
//        return (kelvin - 273.15)
//    }
}
