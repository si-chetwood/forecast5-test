//
//  ForecastVC.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import UIKit

class ForecastVC: UIViewController {
    
    @IBOutlet weak var forecastForSelectedDateCollectionView: UICollectionView!
    @IBOutlet weak var forecastDatesCollectionView: UICollectionView!
    
    private var forecastForSelectedDateCollectionViewAdapter = ForecastForSelectedDateCollectionViewAdapter()
    private var forecastDatesCollectionViewAdapter = ForecastDatesCollectionViewAdapter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupForecastForSelectedDateCollectionView()
        self.setupForecastDatesCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setupForecastDatesCollectionView() {
        self.forecastDatesCollectionViewAdapter.set(collectionView: self.forecastDatesCollectionView, dailyForecastChanged: { (selectedDailyForecast) in
            self.forecastForSelectedDateCollectionViewAdapter.set(dailyForecast: selectedDailyForecast)
        })
        self.getForecastData()
    }
    
    private func getForecastData() {
        let city = City(name: "London", countryCode: "UK")
        let datastore = ForecastDataStore()
        datastore.getForecast(for: city) { (dailyForecasts) in
            self.forecastDatesCollectionViewAdapter.set(dailyForecasts: dailyForecasts)
        }
    }
    
    private func setupForecastForSelectedDateCollectionView() {
        self.forecastForSelectedDateCollectionViewAdapter.set(collectionView: self.forecastForSelectedDateCollectionView)
    }
    
    @IBAction func previousButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        
    }
}

