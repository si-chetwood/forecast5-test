//
//  Double.swift
//  Forecast5
//
//  Created by Simon on 25/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

extension Double {
    
    func trimAtEnd(digits: Int) -> String? {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = digits
        let number = NSNumber(value: self)
        return formatter.string(from: number)
    }
}
