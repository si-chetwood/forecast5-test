//
//  Int.swift
//  Forecast5
//
//  Created by Simon on 25/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

extension Int {
    
    func minimumDigits(_ digits: Int) -> String? {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = digits
        formatter.maximumFractionDigits = 0
        let number = NSNumber(value: self)
        return formatter.string(from: number)
    }
}
