//
//  Date.swift
//  Forecast5
//
//  Created by Simon on 25/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

extension Date {
    
    func getHour() -> Int? {
        guard let timeZone = TimeZone(abbreviation: "BST") else {
            return 0
        }
        var components = Calendar.current.dateComponents(in: timeZone, from: self)
        return components.hour
    }
    
    func getMinutes() -> Int? {
        guard let timeZone = TimeZone(abbreviation: "BST") else {
            return 0
        }
        var components = Calendar.current.dateComponents(in: timeZone, from: self)
        return components.minute
    }
    
    func getDateWithoutTime() -> Date? {
        guard let timeZone = TimeZone(abbreviation: "UTC") else {
            return nil
        }
        var components = Calendar.current.dateComponents(in: timeZone, from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)
    }
    
    func isSameDay(to toDate: Date) -> Bool {
        guard let timeZone = TimeZone(abbreviation: "BST") else {
            return false
        }
        var selfComponents = Calendar.current.dateComponents(in: timeZone, from: self)
        var toDateComponents = Calendar.current.dateComponents(in: timeZone, from: toDate)
        
        if selfComponents.year == toDateComponents.year,
            selfComponents.month == toDateComponents.month,
            selfComponents.day == toDateComponents.day {
            return true
        }
        return false
    }
}
