//
//  City.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

struct City {
    var name: String
    var countryCode: String
}
