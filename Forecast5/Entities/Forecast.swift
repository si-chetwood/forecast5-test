//
//  Forecast.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

struct DailyForecast {
    var date: Date
    var hourlyForecasts: [HourlyForecast]
}

struct HourlyForecast {
    var hour: Int
    var minutes: Int
    var forecast: Forecast
}

struct Forecast {
    
    struct Weather {
        var displayText: String
        var description: String
    }
    
    struct Clouds {
        var percentage: Double
    }
    
    struct Wind {
        var speed: Double
        var degree: Double
    }
    
    struct Temperature {
        var value: Double
        var minimum: Double
        var maximum: Double
    }
    
    struct Pressure {
        var value: Double
    }
    
    struct Level {
        var sea: Double
        var ground: Double
    }
    
    struct Humidity {
        var value: Double
    }

    var weather: Weather?
    var clouds: Clouds?
    var wind: Wind?
    var temperature: Temperature?
    var pressure: Pressure?
    var level: Level?
    var humidity: Humidity?
}
