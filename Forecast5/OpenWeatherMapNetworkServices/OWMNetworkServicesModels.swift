//
//  OWMNetworkServicesModels.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

struct OWMNetworkServicesModels {
    struct getForecast {
        struct request {
            var city: City
        }
        struct response {
            var rawWeatherForecastData: [String:Any]?
        }
    }
}
