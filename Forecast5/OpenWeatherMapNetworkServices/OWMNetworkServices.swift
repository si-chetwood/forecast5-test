//
//  OWMNetworkServices.swift
//  Forecast5
//
//  Created by Simon on 24/05/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation
import Alamofire

class OWMNetworkServices {
    
    static let shared = OWMNetworkServices()
    
    func getForecast(request: OWMNetworkServicesModels.getForecast.request, responseClosure: @escaping ((OWMNetworkServicesModels.getForecast.response)->Void)) {
        let url = "https://openweathermap.org/data/2.5/forecast"
        let params = self.getForecastParameters(city: request.city)
        Alamofire.request(url, parameters: params).responseJSON(queue: DispatchQueue.main, completionHandler: { (rawResponse) in
            var response = OWMNetworkServicesModels.getForecast.response()
            if let rawWeatherData = rawResponse.result.value as? [String:Any] {
                response.rawWeatherForecastData = rawWeatherData
            }
            responseClosure(response)
        })
    }
    
    private func getForecastParameters(city: City) -> Parameters {
        return [
            "q": "\(city.name),\(city.countryCode)",
            "appid": "b6907d289e10d714a6e88b30761fae22"
        ]
    }
}
