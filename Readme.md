When you run the project on simulator, you will see dates at the bottom and at the top weather forecast for the selected date. If you tap on any date, the forecast above will change and you will be able to scroll them left/right to view forecast at different time  of the day

Could do better with more time
1. Show which date is selected
2. Show nice images for different types of weather
3. Add animation for different types of weather
4. Show a day overview graph for pressure, humidity, temperature etc
5. Show more info like clouds, rain etc...